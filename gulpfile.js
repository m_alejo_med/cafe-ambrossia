'use strict'

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync').create(),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    uglify = require('gulp-uglify'),
    usemin = require('gulp-usemin'),
    rev = require('gulp-rev'),
    cleanCSS = require('gulp-clean-css'),
    flatmap = require('gulp-flatmap'),
    htmlmin = require('gulp-htmlmin');
    
gulp.task('sass', function(){
  return gulp.src('./css/*scss')
             .pipe(sass().on('error', sass.logError))
             .pipe(gulp.dest('./css'))
             .pipe(browserSync.stream());
})

gulp.task('sass:watch', function(){
  gulp.watch('./css/*.scss', sass);
});

gulp.task('browser-sync', function(){
  browserSync.init({
    server: {
      baseDir: './'
    }
  });
  gulp.watch('./css/*.scss', sass);
  gulp.watch('./*.html').on('change', browserSync.reload);
  gulp.watch('./js/*.js').on('change', browserSync.reload);
  gulp.watch('./css/*.css').on('change', browserSync.reload);
  
});

gulp.task('default', gulp.series('browser-sync'));

gulp.task(clean, function(){
  return del(['dist']);
});

gulp.task('copyfonts', function(){
  gulp.src('./node_modules/open-iconic/font/fonts/*.{ttf, woff, eof, svg, eot, otf}*')
    .pipe(gulp.dest('.dist/fonts'));
  gulp.src('./node_modules/font-awesome/fonts/*.{ttf, woff, woff2, eof, svg, eot, otf}*')
    .pipe(gulp.dest('./dist/fonts'))
});


gulp.task('imagemin', function(){
  return gulp.src('./images/*.{png, jpg, jpeg, gif}')
    .pipe(imagemin({optimizationLevel: 3, progressice: true, interlaced: true}))
    .pipe(gulp.dest('dist/images'));
});

gulp.task('usemin', function(){
  return gulp.src('./*.html')
    .pipe(flatmap(function(stream, file){
      return stream
        .pipe(usemin({
          css: [rev()],
          html: [function(){ return htmlmin({collapseWhitespace: true})}],
          js: [uglify(), rev()],
          inlinejs: [uglify()],
          inlinecss: [cleanCss(), 'concat']
        }));
    }))
    .pipe(gulp.dest('dist/'));
});


gulp.task('build', ['clean'], function(){
  gulp.start('copyfonts','imagemin', 'usemin');
});







